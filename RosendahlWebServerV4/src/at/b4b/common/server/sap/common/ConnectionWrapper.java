package at.b4b.common.server.sap.common;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class ConnectionWrapper {

	private Connection sqlConnection;
	private final int RETRYCOUNT = 3;
	private final int SLEEP = 1000;
	
	public ConnectionWrapper(Connection connection) {
		this.sqlConnection = connection;
	}

	public PreparedStatementWrapper prepareStatement(String sql) throws SQLException {
		SQLException ex = null;
		for(int i = 0; i<RETRYCOUNT; i++) {
			ex = null;
			try {
				PreparedStatement ps =  sqlConnection.prepareStatement(sql);
				PreparedStatementWrapper psw = new PreparedStatementWrapper(ps);
				return psw;
			}
			catch(SQLException x) {
				ex = x;
				if(MSSQLUtils.isReconnectRequired(x)) {
					throw new ReconnectRequiredException(x);
				}
			}
			try { Thread.sleep(SLEEP); } catch(Exception _) {}
		}
		throw ex;
	}

	public void close() throws SQLException {
		sqlConnection.close();
	}

}
