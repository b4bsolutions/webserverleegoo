package at.b4b.common.server.sap.common;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class PreparedStatementWrapper {
	private final int RETRYCOUNT = 3;
	private final int SLEEP = 1000;
	private PreparedStatement ps;
	
	public PreparedStatementWrapper(PreparedStatement ps) {
		this.ps = ps;
	}
	
	public void setString(int idx, String param) throws SQLException {
		SQLException ex = null;
		for(int i = 0; i<RETRYCOUNT; i++) {
			ex = null;
			try {
				ps.setString(idx, param);
				break;
			}
			catch(SQLException x) {
				ex = x;
				if(MSSQLUtils.isReconnectRequired(x)) {
					throw new ReconnectRequiredException(x);
				}
			}
			try { Thread.sleep(SLEEP); } catch(Exception _) {}
		}
		if(ex != null) {
			throw ex;
		}
	}
	
	public ResultSet executeQuery() throws SQLException {
		SQLException ex = null;
		for(int i = 0; i<RETRYCOUNT; i++) {
			ex = null;
			try {
				return ps.executeQuery();
			}
			catch(SQLException x) {
				ex = x;
				if(MSSQLUtils.isReconnectRequired(x)) {
					throw new ReconnectRequiredException(x);
				}
			}
			try { Thread.sleep(SLEEP); } catch(Exception _) {}
		}
		throw ex;
	}
	
	public void close() throws SQLException {
		SQLException ex = null;
		for(int i = 0; i<RETRYCOUNT; i++) {
			ex = null;
			try {
				ps.close();
				break;
			}
			catch(SQLException x) {
				ex = x;
				if(MSSQLUtils.isReconnectRequired(x)) {
					throw new ReconnectRequiredException(x);
				}
			}
			try { Thread.sleep(SLEEP); } catch(Exception _) {}
		}
		if(ex != null) {
			throw ex;
		}
	}
	
	public void setEscapeProcessing(boolean b) throws SQLException {
		SQLException ex = null;
		for(int i = 0; i<RETRYCOUNT; i++) {
			ex = null;
			try {
				ps.setEscapeProcessing(b);
				break;
			}
			catch(SQLException x) {
				ex = x;
				if(MSSQLUtils.isReconnectRequired(x)) {
					throw new ReconnectRequiredException(x);
				}
			}
			try { Thread.sleep(SLEEP); } catch(Exception _) {}
		}
		if(ex != null) {
			throw ex;
		}
	}
	
	public void execute() throws SQLException {
		SQLException ex = null;
		for(int i = 0; i<RETRYCOUNT; i++) {
			ex = null;
			try {
				ps.execute();
				break;
			}
			catch(SQLException x) {
				ex = x;
				if(MSSQLUtils.isReconnectRequired(x)) {
					throw new ReconnectRequiredException(x);
				}
			}
			try { Thread.sleep(SLEEP); } catch(Exception _) {}
		}
		if(ex != null) {
			throw ex;
		}
	}
	
	public void setNull(int idx, int stringType) throws SQLException {
		SQLException ex = null;
		for(int i = 0; i<RETRYCOUNT; i++) {
			ex = null;
			try {
				ps.setNull(idx, stringType);
				break;
			}
			catch(SQLException x) {
				ex = x;
				if(MSSQLUtils.isReconnectRequired(x)) {
					throw new ReconnectRequiredException(x);
				}
			}
			try { Thread.sleep(SLEEP); } catch(Exception _) {}
		}
		if(ex != null) {
			throw ex;
		}
	}
	
}
