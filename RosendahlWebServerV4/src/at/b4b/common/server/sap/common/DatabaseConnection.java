package at.b4b.common.server.sap.common;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import at.b4b.common.server.sap.erp.ERPSalesQuote;



public class DatabaseConnection {
//	private static Logger log = Logger.getLogger("DatabaseConnection");
	private static final Logger log = LogManager.getLogger(DatabaseConnection.class.getName());
	
	private static ConnectionWrapper dbConnect(String hostname, String db_userid, String db_password, String db) throws SQLException
	{
		ConnectionWrapper conn = null;
		try {
			final String connectString = "jdbc:sqlserver://" + hostname +";database="+db;
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			Connection sqlConnection = DriverManager.getConnection(connectString, db_userid, db_password);
			conn = new ConnectionWrapper(sqlConnection);
//			log.info("connected");

		} 
		catch(ClassNotFoundException x) {
			throw new SQLException("Class for driver not found");
		}

		return conn;
	}

	private static ConnectionWrapper dbConnection = null;
	
	public synchronized static ConnectionWrapper getConnection() throws SQLException {
		if(dbConnection == null) {
			log.info("doing connected to Leegoo_Test_V6");
			dbConnection = DatabaseConnection.dbConnect("192.168.0.141", "HCI", "Rowelcome2016!", "Leegoo_Test_V6");
//			log.info("doing connected to LeegooDat");
//			dbConnection = DatabaseConnection.dbConnect("192.168.0.141", "HCI", "Rowelcome2016!", "LeegooDat");
		}
		return dbConnection;
	}
	
	public synchronized static ConnectionWrapper reconnect() throws SQLException {
//		log.info("doing reconnect to database");
		try {
			dbConnection.close();
			dbConnection = null;
		}
		catch(Exception _) {}
		dbConnection = null;
		return getConnection();
	}
	
/*	public static void main(String[] args) throws Exception {
		DatabaseConnection connServer = new DatabaseConnection();
		
		//Connection conn = connServer.dbConnect("b4bpendl", "sa", "Welcome99");
		Connection conn = connServer.dbConnect("192.168.0.141", "HCI", "Rowelcome2016!", "Leegoo_Test_V6");
		if(conn!=null) {
			 Statement statement = conn.createStatement();
			
	         String queryString = "select * from dbo.luke2";
	         ResultSet rs = statement.executeQuery(queryString);
	         while (rs.next()) {
	            PrintUtil.safePrintln(rs.getString(1) + ", " + rs.getString(2));
	         }
	         
	         
				Statement insertStat = conn.createStatement();
				
		        String insertString = "INSERT INTO dbo.luke2(id,name) VALUES (" + 1234 + ", '" + "Client test 2" + "')";
		        PrintUtil.safePrintln(insertString);
		        insertStat.execute(insertString);
		        
		        try {insertStat.close();} catch(Exception _) {}
	         
		}
	}*/
}
