package at.b4b.common.server.sap.common;

import java.sql.SQLException;

public class ReconnectRequiredException extends SQLException
{
	private static final long serialVersionUID = 1L;

	public ReconnectRequiredException(final Throwable cause) {
		super("reconnect required");
	    initCause(cause);
	}
}