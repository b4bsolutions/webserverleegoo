package at.b4b.common.server.sap.erp;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Account2 implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7187077128549328560L;
	
	public String InternalId;
	public String CustomerClassification;
	public String LifeCycleStatus;
	public String PriceGroup;
	public String ResponsibleEmployeeId;
	public String AgentExternalId;
	//public PartnerFunctions[] partnerFunctions = new PartnerFunctions[999];
	
	
	@Override
	public String toString() {
		String text = "InternalId <" + InternalId + 
					  "> LifeCycleStatus <" + LifeCycleStatus +
					  "> CustomerClassification <" + CustomerClassification +
					  "> PriceGroup <" + PriceGroup + ">";
//					  "> ResponsibleEmployeeId <" + ResponsibleEmployeeId + 
//					  "> AgentExternalId <" + AgentExternalId +

		return text;
	}
}
