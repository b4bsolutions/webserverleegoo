package at.b4b.common.server.sap.erp;

import java.net.URL;
import java.net.URLClassLoader;
import java.sql.SQLException;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;



public class StartupServlet extends HttpServlet {
	private ERPSalesQuote server;
	private static final Logger log = LogManager.getLogger(StartupServlet.class.getName());
	@Override
	public void init() throws ServletException {
		log.info("--------------------------------------------------------------");
		log.info("------------------------STARTUP-------------------------------");

		try {
			server = new ERPSalesQuote();
//			server.keepAlive();
//			(new Thread(new KeepLiving())).start();
		}
		catch(SQLException e) {
			throw new ServletException(e);
//			log.warn("an exception was thrown", e);
		}
		catch(Exception e) {
			throw new ServletException(e);
//			log.warn("an exception was thrown", e);
		}
		log.info("--------------------------------------------------------------");
		
		super.init();
	}
	
	class KeepLiving implements Runnable {
		public void run() {
			while(true) {
				try {
					Thread.sleep(900 * 1000);
				}
				catch(Exception _) {}
				try {
					server.keepAlive();
				}
				catch(Exception e) {
//					log.warn("an exception was thrown", e);
				}
			}
		}
	}	

}

