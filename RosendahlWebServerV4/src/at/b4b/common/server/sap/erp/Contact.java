package at.b4b.common.server.sap.erp;

import java.io.Serializable;

public class Contact implements Serializable {
	private static final long serialVersionUID = 1445145234632715191L;
	public String AccountInternalId;
	public String InternalId;
	public String FamilyName;
	public String GivenName;
	public String FunctionalTitleName;
	public String DepartmentName;
	public String GenderCode;
	public String AcademicTitleCode;
	public String DefaultEmailURI;
	public String DefaultFacsimile;
	public String DefaultConventionalPhone;
	public String DefaultMobilePhone;
	public boolean Obsolete;
	
	@Override
	public String toString() {
		return "InternalId <" + InternalId + 
   			   "> AccountInternalId <" + AccountInternalId +
			   "> FamilyName <" + FamilyName + 
			   "> GivenName <" + GivenName + 
			   "> Status <" + Obsolete +
			   "> FuncitionalTitleName <" + FunctionalTitleName + 
			   "> DepartmentName <" + DepartmentName + 
			   "> GenderCode <" + GenderCode + 
			   "> AcademicTitleCode <" + AcademicTitleCode +
			   "> DefaultEmailURI <" + DefaultEmailURI +
			   "> DefaultFacsimile <" + DefaultFacsimile +
			   "> DefaultConventionalPhone <" + DefaultConventionalPhone +
			   "> DefaultMobilePhone <" + DefaultMobilePhone +
			   ">";
	}
}