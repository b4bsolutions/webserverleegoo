package at.b4b.common.server.sap.erp;

import java.io.Serializable;

public class Account implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7187077128549328560L;
	
	public String InternalId;
	public String CustomerClassification;
	public String LifeCycleStatus;
	public String FirstLineName;
	public String SecondLineName;
	public String ThirdLineName;
	public String CountryCode;
	public String LanguageCode;
	public String StreetName;
	public String StreetSuffixName;
	public String HouseId;
	public String StreetPostalCode;
	public String City;
	public String RegionCode;
	public String POBoxID;
	public String POBoxPostalCode;
	public String DefaultWebURI;
	public String DefaultEmailURI;
	public String DefaultFacsimile;
	public String DefaultConventionalPhone;
	public String DefaultMobilePhone;
	public String PriceGroup;
	public String MatchCode;
	public String AgentExternalId;
	public String VertriebsMitarbeiterId;
		
	@Override
	public String toString() {
		String text = "InternalId <" + InternalId + 
					  "> LifeCycleStatus <" + LifeCycleStatus +
					  "> CustomerClassification <" + CustomerClassification +
					  "> FirstLineName <" + FirstLineName + 
					  "> SecondLineName <" + SecondLineName + 
					  "> ThirdLineName <" + ThirdLineName + 
					  "> CountryCode <" + CountryCode + 
					  "> StreetName <" + StreetName + 
					  "> StreetSuffixName <" + StreetSuffixName +
					  "> HouseId <" + HouseId +
					  "> StreetPostalCode <" + StreetPostalCode + 
					  "> City <" + City +
					  "> RegionCode <" + RegionCode + 
					  "> POBoxID <" + POBoxID + 
					  "> POBoxPostalCode <" + POBoxPostalCode + 
					  "> DefaultWebURI <" + DefaultWebURI + 
					  "> DefaultEmailURI <" + DefaultEmailURI + 
					  "> DefaultFacsimile <" + DefaultFacsimile + 
					  "> DefaultConventionalPhone <" + DefaultConventionalPhone + 
					  "> DefaultMobilePhone <" + DefaultMobilePhone + 
					  "> PriceGroup <" + PriceGroup + 
  					  "> DefaultMobilePhone <" + DefaultMobilePhone + 
  					  "> AgentExternalId <" + AgentExternalId +
  					  "> VertriebsMitarbeiterId <" + VertriebsMitarbeiterId + 
					  "> MatchCode <" + MatchCode + 
					  ">\n";

		return text;
	}
}
