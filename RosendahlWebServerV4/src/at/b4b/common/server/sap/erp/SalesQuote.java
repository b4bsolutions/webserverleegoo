package at.b4b.common.server.sap.erp;

import java.io.Serializable;

public class SalesQuote implements Serializable {
	public String Name;
	public String Currency;
	public String TotalValue;
	public String Note;
	public String CommissionNumber;
	public String SalesMargin;
	public String OpportunityId;
	public String AccountId;
	public Boolean Valid;
	public String created;
	public String RFQDate;
	public String SubmissionDate;
	public String DocumentType;
	public String Commission;
	public String NetSales;
	
	@Override
	public String toString() {
		return "Name <" + Name +
			   "> Currency <" + Currency +
			   "> TotalValue <" + TotalValue +
			   "> Note <" + Note +
			   "> CommissionNumber <" + CommissionNumber +
			   "> SalesMargin <" + SalesMargin +
			   "> created <" + created +
			   "> RFQDate <" + RFQDate +
			   "> SubmissionDate <" + SubmissionDate +
			   "> DocumentType <" + DocumentType +
			   "> Commission <" + Commission +
			   "> NetSales <" + NetSales +
			   "> Opportunity <" + OpportunityId + 
			   "> AccountId <" + AccountId + 
			   "> Valid <"  + Valid + ">";
	}
}
