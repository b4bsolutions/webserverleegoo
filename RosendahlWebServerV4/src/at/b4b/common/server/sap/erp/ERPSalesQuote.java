package at.b4b.common.server.sap.erp;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


import javax.wsdl.Types;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import at.b4b.common.server.sap.common.ConnectionWrapper;
import at.b4b.common.server.sap.common.DatabaseConnection;
import at.b4b.common.server.sap.common.PreparedStatementWrapper;
import at.b4b.common.server.sap.common.ReconnectRequiredException;

public class ERPSalesQuote  {

	private static final Logger log = LogManager.getLogger(ERPSalesQuote.class.getName());

	private final String ResponseOK = "OK";
	private final int RETRYCOUNT = 3;
	private final int SLEEP = 1000;
	
	private ConnectionWrapper connection;
	
	private void createLogger() {
	}
	
	public ERPSalesQuote() throws SQLException {
		createLogger();
		
		connection = DatabaseConnection.getConnection();
	}
	
	public synchronized void keepAlive() throws SQLException {
		synchronized(connection) {
			
			log.info("keepAlive...");
			ReconnectRequiredException ex = null;
			for(int i=0; i<RETRYCOUNT; i++) {
				ex = null;
				PreparedStatementWrapper ps = null;
				try {
//					if(i==0) {
//						PrintUtil.safePrintln("throw Exception");
//						throw new ReconnectRequiredException(new SQLException("test"));
//					};
					String sql = "select * from view_HCI where DATST2 >= ?";
					ps = connection.prepareStatement(sql);
					SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
					String time = sdf.format(new Date(System.currentTimeMillis()));
					ps.setString(1, time);
					ResultSet rs = ps.executeQuery();
					break;
				}
				catch(ReconnectRequiredException e) {
					ex = e;
					connection = DatabaseConnection.reconnect();
				}
				catch(SQLException e) {
					log.info("hier");
					log.warn("an exception was thrown", e);
					
					throw e;
				}
				finally {
					try {ps.close();} catch(Exception _) {}
				}
				log.info("retry...");
				try {Thread.sleep(SLEEP);} catch(Exception _) {}
			}
			if(ex != null) {
				log.error("excpetion ", ex);
				throw ex;
			}
		}
	}
	
	public synchronized SalesQuote[] requestSalesQuotesByTimestamp(Date lastRequested) throws SQLException {
		synchronized(connection) {
			ReconnectRequiredException ex = null;
			ArrayList<SalesQuote> quotes = new ArrayList<SalesQuote>();
			PreparedStatementWrapper ps = null;

			for(int i=0; i<RETRYCOUNT; i++) {
				ex = null;
				try {
					String sql = "select * from view_HCI where DATST2 >= ? and OpportunityID is not null order by DATST2";
					ps = connection.prepareStatement(sql);
					ps.setEscapeProcessing(true);
			
					
					SimpleDateFormat from = new SimpleDateFormat("yyyyMMdd");
					SimpleDateFormat dest = new SimpleDateFormat("yyyy-MM-dd");
					
					SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		//			sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
					String time = sdf.format(lastRequested);
					
					log.info("requestSalesQuotesByTimestamp: " + time);
					ps.setString(1, time);
				
					ResultSet rs = ps.executeQuery();
					while(rs.next()) {
						SalesQuote quote = new SalesQuote();
		//				quote.AccountId = AccountId;
						String opp = rs.getString("OpportunityID");
						if(opp != null) {
							quote.OpportunityId = opp.length() > 0 ? opp : null;
						}
						
						quote.CommissionNumber = rs.getString("COMMISNO");
						quote.Name = rs.getString("BELEGID");
						quote.Currency = rs.getString("CURZIEL");
						quote.TotalValue = rs.getString("E01");
						quote.Note = rs.getString("NOTIZ");
						quote.SalesMargin = rs.getString("SMVOL_P");
						String status = rs.getString("STATUSGK");
						if(status != null) {
							quote.Valid = status.equals("002") ? true : false;
						}
						
						
						quote.created = rs.getString("ERSTDATUM");
						if(quote.created != null) {
							try {
								Date d = from.parse(quote.created);
								quote.created = dest.format(d);// + "T00:00:00";
							}
							catch(java.text.ParseException x) {
								log.info("cannot parse input " + quote.created);
							}
						}
						quote.RFQDate = rs.getString("ANFRDATUM");
						if(quote.RFQDate != null) {
							try {
								Date d = from.parse(quote.RFQDate);
								quote.RFQDate = dest.format(d);// + "T00:00:00";
							}
							catch(java.text.ParseException x) {
								log.info("cannot parse input " + quote.RFQDate);
							}
						}
						quote.SubmissionDate = rs.getString("WVDATUM");
						if(quote.SubmissionDate != null) {
							try {
								Date d = from.parse(quote.SubmissionDate);
								quote.SubmissionDate = dest.format(d) ;//+ "T00:00:00";
							}
							catch(java.text.ParseException x) {
								log.info("cannot parse input " + quote.SubmissionDate);
							}
						}
						quote.DocumentType = rs.getString("BELEGART");
						quote.Commission = rs.getString("COMMISSION");
						quote.NetSales = rs.getString("Netsales");
						quotes.add(quote);
						
						log.info("loaded quote: " + quote.toString());
					}
					break;
				}
				catch(ReconnectRequiredException e) {
					ex = e;
					connection = DatabaseConnection.reconnect();
				}
				catch(SQLException e) {
					log.warn("an exception was thrown", e);
					throw e;
				}
				finally {
					try {ps.close();} catch(Exception _) {}
				}
				log.info("retry...");
				try {Thread.sleep(SLEEP);} catch(Exception _) {}
			}
			if(ex != null) {
				log.error("excpetion ", ex);
				throw ex;
			}
	//		quotes.add(quote);
	//		quotes.add(quote2);
	//		PrintUtil.safePrintln(quote.toString());
	//		PrintUtil.safePrintln(quote2.toString());
			
			
			SalesQuote[] salesQuotes = new SalesQuote[quotes.size()];
			salesQuotes = quotes.toArray(salesQuotes);
			return salesQuotes;
			
		}
	}
	
	public synchronized SalesQuote[] requestSalesQuotes(String OpportunityId, String AccountId, Date LastRequested) throws SQLException {
		synchronized(connection) {
			log.info("Requesting data for Opportunity " + OpportunityId + " timestamp " + LastRequested);
	
			ReconnectRequiredException ex = null;
			ArrayList<SalesQuote> quotes = new ArrayList<SalesQuote>();
			PreparedStatementWrapper ps = null;
			SimpleDateFormat from = new SimpleDateFormat("yyyyMMdd");
			SimpleDateFormat dest = new SimpleDateFormat("yyyy-MM-dd");
			for(int i=0; i<RETRYCOUNT; i++) {
				ex = null;

				try {
					String sql = "select * from view_HCI where OpportunityID = ?";
					ps = connection.prepareStatement(sql);
					ps.setEscapeProcessing(true);
			
					ps.setString(1, OpportunityId);
					
					ResultSet rs = ps.executeQuery();
					while(rs.next()) {
						SalesQuote quote = new SalesQuote();
						quote.AccountId = AccountId;
						quote.OpportunityId = OpportunityId;
						
						quote.CommissionNumber = rs.getString("COMMISNO");
						quote.Name = rs.getString("BELEGID");
						quote.Currency = rs.getString("CURZIEL");
						quote.TotalValue = rs.getString("E01");
						quote.Note = rs.getString("NOTIZ");
						quote.SalesMargin = rs.getString("SMVOL_P");
						quote.Valid = rs.getString("STATUSGK").equals("002") ? true : false;
						quote.created = rs.getString("ERSTDATUM");
						if(quote.created != null) {
							try {
								Date d = from.parse(quote.created);
								quote.created = dest.format(d);// + "T00:00:00";
							}
							catch(java.text.ParseException x) {
								log.info("cannot parse input " + quote.created);
							}
						}
						quote.RFQDate = rs.getString("ANFRDATUM");
						if(quote.RFQDate != null) {
							try {
								Date d = from.parse(quote.RFQDate);
								quote.RFQDate = dest.format(d);// + "T00:00:00";
							}
							catch(java.text.ParseException x) {
								log.info("cannot parse input " + quote.RFQDate);
							}
						}
						quote.SubmissionDate = rs.getString("WVDATUM");
						if(quote.SubmissionDate != null) {
							try {
								Date d = from.parse(quote.SubmissionDate);
								quote.SubmissionDate = dest.format(d);// + "T00:00:00";
							}
							catch(java.text.ParseException x) {
								log.info("cannot parse input " + quote.SubmissionDate);
							}
						}
						quote.DocumentType = rs.getString("BELEGART");
						quote.Commission = rs.getString("COMMISSION");
						quote.NetSales = rs.getString("Netsales");
						quotes.add(quote);
						
						log.info("loaded quote: " + quote.toString());
					}
					break;
				}
				catch(ReconnectRequiredException e) {
					ex = e;
					connection = DatabaseConnection.reconnect();
				}
				catch(SQLException e) {
					log.warn("an exception was thrown", e);
					throw e;
				}
				finally {
					try {ps.close();} catch(Exception _) {}
				}
				log.info("retry...");
				try {Thread.sleep(SLEEP);} catch(Exception _) {}
			}
			
			if(ex != null) {
				log.error("excpetion ", ex);
				throw ex;
			}
	//		quotes.add(quote);
	//		quotes.add(quote2);
	//		PrintUtil.safePrintln(quote.toString());
	//		PrintUtil.safePrintln(quote2.toString());
			
			
			SalesQuote[] salesQuotes = new SalesQuote[quotes.size()];
			salesQuotes = quotes.toArray(salesQuotes);
			return salesQuotes;

		}
	}

	/*			String sql = "EXEC LGB_ERROR_TEST ? ";
	ps = connection.prepareStatement(sql);
	ps.setEscapeProcessing(true);
	ps.setString(1, account.InternalId);
	PrintUtil.safePrintln("statement LGB_Account_Update executed");*/
	
	public synchronized String createOrUpdateAccountMini(Account2 account, PartnerFunctions[] functions) throws SQLException {
		synchronized(connection) {
			log.info("ACCOUNT: " + account.toString());
			log.info("FUNCTIONS: ");
			String ResponsibleEmployeeId = null;
			String AgentExternalId = null;
			
			if(functions != null) { //
			
				for(int i = 0; i< functions.length; i++) {
					PartnerFunctions f = functions[i];
					if(f != null) {
						log.info(f.toString());
						if("ZA".equals(f.Code)) {
							AgentExternalId = f.Id;
						}
						if("VE".equals(f.Code)) {
							ResponsibleEmployeeId = f.Id;
						}
					}
				}
			}
			
			ReconnectRequiredException ex = null;
		
			PreparedStatementWrapper ps = null;
			
			for(int i=0; i<RETRYCOUNT; i++) {
				ex = null;
				try {
					
					String SPsql = "EXEC LGB_Account_Update "
							+ "?, " //InterneID
							+ "?, " // CustomerClassification
							+ "?, " // LifeCycleStatus
							+ "?, " // Pricegroup
							+ "?, " // ResponsibleEmployeeId
							+ "?"; // AgentExternalId
						   
					ps = connection.prepareStatement(SPsql);
					ps.setEscapeProcessing(true);
			
					ps.setString(1, account.InternalId);
					ps.setString(2, account.CustomerClassification);
					ps.setString(3, account.LifeCycleStatus);
					ps.setString(4, account.PriceGroup);
					
					if(ResponsibleEmployeeId != null) {
						ps.setString(5, ResponsibleEmployeeId);
					}
					else {
						ps.setNull(5, Types.STRING_TYPE);
					}
					if(AgentExternalId != null) {
						ps.setString(6, AgentExternalId);
					}
					else {
						ps.setNull(6, Types.STRING_TYPE);
					}
					
					ps.execute();
					
					log.info("statement LGB_Account_Update executed");
					break;
				}
				catch(ReconnectRequiredException e) {
					ex = e;
					connection = DatabaseConnection.reconnect();
				}
				catch(SQLException e) {
					log.warn("an exception was thrown", e);
					throw e;
				}
				finally {
					try { ps.close(); } catch(Exception _) {}
				}
				log.info("retry...");
				try {Thread.sleep(SLEEP);} catch(Exception _) {}
			}
			if(ex != null) {
				log.error("excpetion ", ex);
				throw ex;
			}
		}
		return ResponseOK;
	}
	
	public synchronized String createOrUpdateAccount(Account account) throws SQLException
	{
		synchronized(connection) {
			log.info("ACCOUNT: " + account.toString());
			ReconnectRequiredException ex = null;
		
			PreparedStatementWrapper ps = null;
			
			for(int i=0; i<RETRYCOUNT; i++) {
				ex = null;
				try {
					String SPsql = "EXEC LGB_Account "
							+ "?, " //InterneID
							+ "?, "
							+ "?, "
							+ "?, " // Name1
							+ "?, " // Name2
							+ "?, " // Name3
							+ "?, " // MatchCode
							+ "?, " // Straße1
							+ "?, " // Straße2
							+ "?, " // Ort
							+ "?, " // PLZ
							+ "?, " // Land
							+ "?, " // Bundesland
							+ "?, " // Postfach
							+ "?, " // Postfach PLZ
							+ "?, " // Sprache
							+ "?, " // Website
							+ "?, " // Status
							+ "?, " // Kundenklasse
							+ "?, " // 
							+ "?, " // StraßenNr
							+ "?, " // Telefon
							+ "?, " // Mobil
							+ "?, " // Fax
							+ "?, " // Email
							+ "?"; // Pricegroup
			//TODO:
			//				+ "?";  // VertreterId
					
					ps = connection.prepareStatement(SPsql);
					ps.setEscapeProcessing(true);
			
					ps.setString(1, account.InternalId);
					ps.setNull(2, Types.STRING_TYPE);
					ps.setNull(3, Types.STRING_TYPE);
					
					ps.setString(4, account.FirstLineName);
					ps.setString(5, account.SecondLineName);
					ps.setString(6, account.ThirdLineName);
					ps.setString(7, account.MatchCode);
		
					if(account.StreetName != null && account.StreetName.length()>80) {
						ps.setString(8, account.StreetName.substring(0,79));
//						ps.setString(9, account.StreetName.substring(80));
						
					}
					else {
						ps.setString(8, account.StreetName);
//						ps.setNull(9, Types.STRING_TYPE);
					}
					
					if(account.StreetSuffixName != null) {
						ps.setString(9, account.StreetSuffixName);
					}
					else {
						ps.setNull(9, Types.STRING_TYPE);						
					}
					
		/*			String streetNumber = null;
					if(account.StreetName != null) {
						int idx = account.StreetName.lastIndexOf(' ');
						if(idx != -1) {
							streetNumber = account.StreetName.substring(idx+1);
							account.StreetName = account.StreetName.substring(0, idx);
							if(account.StreetName != null && account.StreetName.length()>80) {
								ps.setString(8, account.StreetName.substring(0,79));
								ps.setString(9, account.StreetName.substring(80));
								
							}
							else {
								ps.setString(8, account.StreetName);
								ps.setString(9, "NULL");
							}
						}
						else {
							ps.setString(8, account.StreetName);
							ps.setString(9, "NULL");
						}
					}
					else {
						ps.setString(8, account.StreetName);
						ps.setString(9, "NULL");
					}*/
					ps.setString(10, account.City);
					ps.setString(11, account.StreetPostalCode);
					ps.setString(12, account.CountryCode);
					ps.setString(13, account.RegionCode);
					ps.setString(14, account.POBoxID);
					ps.setString(15, account.POBoxPostalCode);
					ps.setString(16, account.LanguageCode);
					ps.setString(17, account.DefaultWebURI);
					ps.setString(18, account.LifeCycleStatus);
					ps.setString(19, account.CustomerClassification);
					ps.setNull(20, Types.STRING_TYPE);
					
		//			PrintUtil.safePrintln("streetNumber: " + streetNumber);
		//			if(streetNumber != null) {
						ps.setString(21, account.HouseId);
		//			}
		//			else {
		//				ps.setString(21, "NULL");
		//			}
					ps.setString(22, account.DefaultConventionalPhone);
					ps.setString(23, account.DefaultMobilePhone);
					ps.setString(24, account.DefaultFacsimile);
					ps.setString(25, account.DefaultEmailURI);
					ps.setString(26, account.PriceGroup);
	
					ps.execute();
					
					log.info("statement LGB_Account executed");
					break;
		
				}
				catch(ReconnectRequiredException e) {
					ex = e;
					connection = DatabaseConnection.reconnect();
				}
				catch(SQLException e) {
					log.warn("an exception was thrown", e);
					throw e;
				}
				finally {
					try { ps.close(); } catch(Exception _) {}
				}
				log.info("retry...");
				try {Thread.sleep(SLEEP);} catch(Exception _) {}
			}
			if(ex != null) {
				log.error("excpetion ", ex);
				throw ex;
			}
			return ResponseOK;
		}
	}
	
	public synchronized String createOrUpdateContact(Contact[] contacts) throws SQLException {
		synchronized(connection) {
			if(contacts != null) {
				log.info("CONTACT: \n");
				for(Contact c : contacts) {
					log.info("contact " + c.toString() + "\n");
					ReconnectRequiredException ex = null;
					for(int i=0; i<RETRYCOUNT; i++) {
						ex = null;
						PreparedStatementWrapper ps = null;
						try {
							String SPsql = "EXEC LGB_Contact "
									+ "?, " // InterneID
									+ "?, " // AccountInternalId
									+ "?, " // NULL
									+ "?, " // NULL
									+ "?, " // Name
									+ "?, " // Vorname
									+ "?, " // Titel
									+ "?, " // Status
									+ "?, " // Geschlecht
									+ "?, " // FunctionalTitle
									+ "?, " // Department
									+ "?, " // Fax
									+ "?, " // Telefon
									+ "?, " // Mobil
									+ "?"; 	// Email
		
							
							ps = connection.prepareStatement(SPsql);
							ps.setEscapeProcessing(true);
					
							ps.setString(1, c.InternalId);
							ps.setString(2, c.AccountInternalId);
							ps.setNull(3, Types.STRING_TYPE);
							ps.setNull(4, Types.STRING_TYPE);
							ps.setString(5, c.FamilyName);
							ps.setString(6, c.GivenName);
							ps.setString(7, c.AcademicTitleCode);
							if(c.Obsolete) {
								ps.setString(8, "0");
							}
							else {
								ps.setString(8, "1");
							}
							ps.setString(9, c.GenderCode);
							ps.setString(10, c.FunctionalTitleName);
							ps.setString(11, c.DepartmentName);
							ps.setString(12, c.DefaultFacsimile);
							ps.setString(13, c.DefaultConventionalPhone);
							ps.setString(14, c.DefaultMobilePhone);
							ps.setString(15, c.DefaultEmailURI);
	
							ps.execute();
	
							
							log.info("statement LGB_Contact executed");
							break;
						}
						catch(ReconnectRequiredException e) {
							ex = e;
							connection = DatabaseConnection.reconnect();
						}
						catch(Exception e) {
							log.warn("an exception was thrown", e);
						}
						finally {
							try { ps.close(); } catch(Exception _) {}
						}
						log.info("retry...");
						try {Thread.sleep(SLEEP);} catch(Exception _) {}
					}
					if(ex != null) {
						log.error("excpetion ", ex);
						throw ex;
					}

				}
			}
			
			return ResponseOK;		

		}
	}

	/*		try {
	 Statement statement = connection.createStatement();
	
    String queryString = "select * from Z_SAP_SBU";
    ResultSet rs = statement.executeQuery(queryString);
    while (rs.next()) {
       PrintUtil.safePrintln(rs.getString(1) + ", " + rs.getString(2) + ", " + rs.getString(3));
    }
	
}
catch(Exception e) {
	e.printStackTrace(System.out);
}
*/	
}